'use strict';
// fixed svg show
//-----------------------------------------------------------------------------
svg4everybody();

// checking if element for page
//-----------------------------------------------------------------------------------
function isOnPage(selector) {
	return ($(selector).length) ? $(selector) : false;
}


// $(window).scroll(function(){
// 	var sticky = $('.sticky'),
// 		scroll = $(window).scrollTop();

// 	if (scroll >= 100) sticky.addClass('fixed');
// 	else sticky.removeClass('fixed');
//   });


$("header").headroom({
	"offset": 100,
	"tolerance": 5,
	"classes": {
		"initial": "animated",
		"pinned": "slideDown",
		"unpinned": "slideUp"
	}
});

$(document).ready(function () {
	$("a.scrollLink").click(function (event) {
		event.preventDefault();
		$("html, body").animate({ scrollTop: $($(this).attr("href")).offset().top }, 500);
	});
});



var boxbuy = $('[data-remodal-id=box-buy]').remodal();
var thanks = $('[data-remodal-id=thanks]').remodal();

function submitform()
{
 if(document.myform.onsubmit())
 {//this check triggers the validations
    document.myform.submit();
 }
}


$('.phone').mask('+38(000) 000-0000');

$('form.box-buy').submit(function(e){
	e.preventDefault();
	if ($('form.box-buy').valid() ){
		
		boxbuy.close();
		thanks.open();
	}
})