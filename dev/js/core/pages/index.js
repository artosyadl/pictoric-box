$(function () {

  var $frame = $('#centered');
  var $wrap = $frame.parent();

  // Call Sly on frame
  $frame.sly({
    horizontal: 1,
    itemNav: 'centered',
    smart: 1,
    activateOn: 'click',
    mouseDragging: 1,
    touchDragging: 1,
    releaseSwing: 1,
    startAt: 0,
    scrollBar: $wrap.find('.scrollbar'),
    scrollBy: 1,
    speed: 300,
    elasticBounds: 1,
    easing: 'easeOutExpo',
    dragHandle: 1,
    dynamicHandle: 1,
    clickBar: 1,

    // Buttons
    prev: $wrap.find('.prev'),
    next: $wrap.find('.next')
  });



  var $frame1 = $('#crazy');
  var $wrap1 = $frame1.parent();

  $frame1.sly({
    horizontal: 1,
    itemNav: 'basic',
    smart: 1,
    activateOn: 'click',
    mouseDragging: 1,
    touchDragging: 1,
    releaseSwing: 1,
    startAt: 0,
    scrollBar: $wrap1.find('.scrollbar'),
    scrollBy: 1,
    activatePageOn: 'click',
    speed: 300,
    elasticBounds: 1,
    easing: 'easeOutExpo',
    dragHandle: 1,
    dynamicHandle: 1,
    clickBar: 1,

    prevPage: $wrap1.find('.js-prev-crazy'),
    nextPage: $wrap1.find('.js-next-crazy')
  });



  var $frameGallery = $('#gallery');
  var $wrapGallery = $frameGallery.parent();

  // Call Sly on frame
  $frameGallery.sly({
    horizontal: 1,
    itemNav: 'centered',
    smart: 1,
    activateOn: 'click',
    mouseDragging: 1,
    touchDragging: 1,
    releaseSwing: 1,
    startAt: 0,
    scrollBar: $wrapGallery.find('.scrollbar'),
    scrollBy: 1,
    speed: 300,
    elasticBounds: 1,
    easing: 'easeOutExpo',
    dragHandle: 1,
    dynamicHandle: 1,
    clickBar: 1,

    // Buttons
    prevPage: $wrapGallery.find('.js-prev-gallery'),
    nextPage: $wrapGallery.find('.js-next-gallery')
  });

  function parallaxIt(e, target, movement) {
    var $this = $(".stub");
    var relX = e.pageX - $this.offset().left;
    var relY = e.pageY - $this.offset().top;
  
    TweenMax.to(target, 1, {
      x: (relX - $this.width() / 2) / $this.width() * movement,
      y: (relY - $this.height() / 2) / $this.height() * movement
    });
  }
  
  $(".stub").mousemove(function(e) {
    parallaxIt(e, ".plax-3", -20);
    parallaxIt(e, ".plax-1", 20);
    
  });
});


