'use strict';
// fixed svg show
//-----------------------------------------------------------------------------
svg4everybody();

// checking if element for page
//-----------------------------------------------------------------------------------
function isOnPage(selector) {
	return ($(selector).length) ? $(selector) : false;
}


// $(window).scroll(function(){
// 	var sticky = $('.sticky'),
// 		scroll = $(window).scrollTop();

// 	if (scroll >= 100) sticky.addClass('fixed');
// 	else sticky.removeClass('fixed');
//   });


$("header").headroom({
	"offset": 100,
	"tolerance": 5,
	"classes": {
		"initial": "animated",
		"pinned": "slideDown",
		"unpinned": "slideUp"
	}
});

$(document).ready(function () {
	$("a.scrollLink").click(function (event) {
		event.preventDefault();
		$("html, body").animate({ scrollTop: $($(this).attr("href")).offset().top }, 500);
	});
});



var boxbuy = $('[data-remodal-id=box-buy]').remodal();
var thanks = $('[data-remodal-id=thanks]').remodal();

function submitform()
{
 if(document.myform.onsubmit())
 {//this check triggers the validations
    document.myform.submit();
 }
}


$('.phone').mask('+38(000) 000-0000');

$('form.box-buy').submit(function(e){
	e.preventDefault();
	if ($('form.box-buy').valid() ){
		
		boxbuy.close();
		thanks.open();
	}
})
// custom jQuery validation
//-----------------------------------------------------------------------------------
var validator = {
    init: function () {
      $('form').each(function () {
        var $form = $(this);
        var name = $form.attr('name');
        if (validator.valitatorRules.hasOwnProperty(name) || $form.hasClass('js-validate')) {
          var rules = validator.valitatorRules[name];
          $form.validate({
            rules:          rules,
            errorElement:    'b',
            errorClass:      'error',
            focusInvalid:    true,
            focusCleanup:    false,
            errorPlacement: function (error, element) {
              validator.setError($(element), error);
            },
            highlight: function (element, errorClass, validClass) {
              var $el = validator.defineElement($(element));
              if ($(element).attr('type') == 'file'){
                setTimeout(function () {
                  $(element).parents('label').find('b.error').addClass('file-error');
                  $(element).parents('.img-load').after($(element).parents('label').find('b.error'));
                }, 100)
              } else {
                if ($el){
                  $el.closest('.el-text-fel').removeClass(validClass).addClass(errorClass);
                }
              }
            },
            unhighlight: function (element, errorClass, validClass) {
              var $el = validator.defineElement($(element));
              if ($el){
                $el.closest('.el-text-fel').removeClass(errorClass).addClass(validClass);
              }
            },
            onfocusout: function(element) {
              // var $el = validator.defineElement($(element));
              // $el.valid();
            },
            messages: validator.messages
          });
        }
      });
    },
    setError: function ($el, message) {
      $el = this.defineElement($el);
      if ($el) this.domWorker.error($el, message);
    },
    defineElement: function ($el) {
      return $el;
    },
    domWorker: {
      error: function ($el, message) {
        $el.closest('.el-text-fel').addClass('error');
        $el.after(message);
      }
    },
    messages: {
      'field_test': {
        required: 'This field is required.'
      }
    },
    valitatorRules: {
      'form_test': {
        'field_test': {
          required: true
        }
      }
    }
  };
  
  validator.init();
  
  // validate by data attribute
  //-----------------------------------------------------------------------------------
  (function(){
    // add to validate form class 'js-validate'
    // add to validate field data-valid="test"
    //-----------------------------------------------------------------------------------
    var rules = {
      'name': {
        required: true,
        minlength: 2,
        maxlength: 255,
        messages: {
          required: "Это поле обезательное для заполнения",
          minlength: 'Минимум 2 символа',
          maxlength: 'Максимально 255 символов'
        }
      },
      'phone': {
        required: true,
        digits: true,
        minlength: 17,
        maxlength: 255,
        messages: {
          required: "Это поле обезательное для заполнения",
          minlength: 'Минимум 12 символа',
          maxlength: 'Максимально 255 символов',
          digits: 'Вводите только цифры'
        }
      },
      'company': {
        minlength: 2,
        maxlength: 255,
        messages: {
          minlength: 'Must have at least 2 characters!',
          maxlength: 'No more than 255 characters.'
        }
      },
      'message': {
        required: true,
        minlength: 10,
        maxlength: 95,
        messages: {
          required: "Это поле обезательное для заполнения",
          minlength: 'Must have at least 10 characters!',
          maxlength: 'No more than 95 characters.'
        }
      },
      'email': {
        required: false,
        email: true,
        maxlength: 255,
        messages: {
          required: "Это поле обезательное для заполнения",
          email: 'Неправильный e-mail!',
          maxlength: 'Максимально 255 символов'
        }
      },
      'file': {
        extension: "jpeg|jpg|png|doc|docx|pdf",
        filesize: 30720000,
        messages: {
          extension: 'Invalid extension jpeg|jpg|png|doc|docx|pdf',
          filesize: 'File must be less than 30mb.'
        }
      }
    };
  
    for (var ruleName in rules) {
      $('[data-valid=' + ruleName + ']').each(function(){
        $(this).rules('add', rules[ruleName]);
      });
    };
  }());
  
  // custom rules
  //-----------------------------------------------------------------------------------
  $.validator.addMethod("email", function (value) {
    if (value == '') return true;
    var regexp = /[a-zA-Zа-яА-ЯёЁ0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-zA-Zа-яА-ЯёЁ0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-zA-Zа-яА-ЯёЁ0-9](?:[a-zA-Zа-яА-ЯёЁ0-9-]*[a-zA-Zа-яА-ЯёЁ0-9])?\.)+[a-zA-Zа-яА-ЯёЁ0-9](?:[a-zA-Zа-яА-ЯёЁ0-9-]*[a-zA-Zа-яА-ЯёЁ0-9])?/;
    return regexp.test(value);
  });
  $.validator.addMethod("extension", function (value, element, param) {
    param = typeof param === "string" ? param.replace(/,/g, '|') : "png|jpe?g|gif";
    return this.optional(element) || value.match(new RegExp(".(" + param + ")$", "i"));
  });
  $.validator.addMethod('filesize', function (value, element, param) {
    return this.optional(element) || (element.files[0].size <= param)
  });
  $.validator.addMethod("letters", function(value, element) {
    return this.optional(element) || /^[^1-9!@#\$%\^&\*\(\)\[\]:;,.?=+_<>`~\\\/"]+$/i.test(value);
  });
  $.validator.addMethod("digits", function(value, element) {
    return this.optional(element) || /^(\+?\d+)?\s*(\(\d+\))?[\s-]*([\d-]*)$/i.test(value);
  });
  $.validator.addMethod("valueNotEquals", function(value, element, arg){
    return arg != value;
  }, "Value must not equal arg.");
  $.validator.addMethod( "require_from_group", function( value, element, options ) {
    var $fields = $( options[ 1 ], element.form ),
      $fieldsFirst = $fields.eq( 0 ),
      validator = $fieldsFirst.data( "valid_req_grp" ) ? $fieldsFirst.data( "valid_req_grp" ) : $.extend( {}, this ),
      isValid = $fields.filter( function() {
        return validator.elementValue( this );
      } ).length >= options[ 0 ];
    $fieldsFirst.data( "valid_req_grp", validator );
    if ( !$( element ).data( "being_validated" ) ) {
      $fields.data( "being_validated", true );
      $fields.each( function() {
        validator.element( this );
      } );
      $fields.data( "being_validated", false );
    }
    return isValid;
  }, $.validator.format( "Please fill at least {0} of these fields." ) );
  
$(function () {

  var $frame = $('#centered');
  var $wrap = $frame.parent();

  // Call Sly on frame
  $frame.sly({
    horizontal: 1,
    itemNav: 'centered',
    smart: 1,
    activateOn: 'click',
    mouseDragging: 1,
    touchDragging: 1,
    releaseSwing: 1,
    startAt: 0,
    scrollBar: $wrap.find('.scrollbar'),
    scrollBy: 1,
    speed: 300,
    elasticBounds: 1,
    easing: 'easeOutExpo',
    dragHandle: 1,
    dynamicHandle: 1,
    clickBar: 1,

    // Buttons
    prev: $wrap.find('.prev'),
    next: $wrap.find('.next')
  });



  var $frame1 = $('#crazy');
  var $wrap1 = $frame1.parent();

  $frame1.sly({
    horizontal: 1,
    itemNav: 'basic',
    smart: 1,
    activateOn: 'click',
    mouseDragging: 1,
    touchDragging: 1,
    releaseSwing: 1,
    startAt: 0,
    scrollBar: $wrap1.find('.scrollbar'),
    scrollBy: 1,
    activatePageOn: 'click',
    speed: 300,
    elasticBounds: 1,
    easing: 'easeOutExpo',
    dragHandle: 1,
    dynamicHandle: 1,
    clickBar: 1,

    prevPage: $wrap1.find('.js-prev-crazy'),
    nextPage: $wrap1.find('.js-next-crazy')
  });



  var $frameGallery = $('#gallery');
  var $wrapGallery = $frameGallery.parent();

  // Call Sly on frame
  $frameGallery.sly({
    horizontal: 1,
    itemNav: 'centered',
    smart: 1,
    activateOn: 'click',
    mouseDragging: 1,
    touchDragging: 1,
    releaseSwing: 1,
    startAt: 0,
    scrollBar: $wrapGallery.find('.scrollbar'),
    scrollBy: 1,
    speed: 300,
    elasticBounds: 1,
    easing: 'easeOutExpo',
    dragHandle: 1,
    dynamicHandle: 1,
    clickBar: 1,

    // Buttons
    prevPage: $wrapGallery.find('.js-prev-gallery'),
    nextPage: $wrapGallery.find('.js-next-gallery')
  });

  function parallaxIt(e, target, movement) {
    var $this = $(".stub");
    var relX = e.pageX - $this.offset().left;
    var relY = e.pageY - $this.offset().top;
  
    TweenMax.to(target, 1, {
      x: (relX - $this.width() / 2) / $this.width() * movement,
      y: (relY - $this.height() / 2) / $this.height() * movement
    });
  }
  
  $(".stub").mousemove(function(e) {
    parallaxIt(e, ".plax-3", -20);
    parallaxIt(e, ".plax-1", 20);
    
  });
});


